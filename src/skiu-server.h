#pragma once

#include "skiu-client.h"

G_BEGIN_DECLS

void skiu_server_listener_add (SkiuClient *client);

G_END_DECLS
