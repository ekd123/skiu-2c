#pragma once

#include <glib.h>

/**
 * SkProtocolDocType:
 * @SK_TYPE_INVALID: not a valid document
 * @SK_TYPE_NEW_ACCOUNT: create a new account
 * @SK_TYPE_JOIN_ROOM: join a room
 * @SK_TYPE_QUIT_ROOM: quit a room
 * @SK_TYPE_STATUS_GOONLINE: go online, or confirm online
 * @SK_TYPE_STATUS_GOAWAY:
 * @SK_TYPE_STATUS_GOBUSY:
 * @SK_TYPE_STATUS_GOTALKTOME:
 * @SK_TYPE_STATUS_GOINVISIBLE:
 * @SK_TYPE_STATUS_GOOFFLINE:
 * @SK_TYPE_MESSAGE: a message
 * @SK_TYPE_ERROR: an error
 * 
 * Indicate the type of a document.
 */
typedef enum 
{
	SK_TYPE_INVALID = 0, 
	SK_TYPE_NEW_ACCOUNT = 1, 

	SK_TYPE_JOIN_ROOM = 10,
	SK_TYPE_QUIT_ROOM = 11,

	SK_TYPE_STATUS_GOONLINE = 100, 
	SK_TYPE_STATUS_GOAWAY, 
	SK_TYPE_STATUS_GOBUSY, 
	SK_TYPE_STATUS_GOTALKTOME, 
	SK_TYPE_STATUS_GOINVISIBLE, 
	SK_TYPE_STATUS_GOOFFLINE, 

	SK_TYPE_MESSAGE = 400, 

	SK_TYPE_ERROR = 500, 
} SkProtocolDocType;

typedef enum 
{
	SK_ERROR_VERSION_UNSUPPORTED = 100, /* Version not supported */
	SK_ERROR_AUTH_FAILED, /* Authentication failed */
	SK_ERROR_BAD_ID, /* Bad ID given */
	SK_ERROR_SERVER_QUIT, /* Server has quited */

	SK_ERROR_INTERNAL, /* Internal errors */
	SK_ERROR_ID_TAKEN, /* This ID has been taken already */
	SK_ERROR_BAD_DOC, /* Bad document given */
} SkProtocolErrorNumber;

G_BEGIN_DECLS

gboolean skiu_protocol_supported (gchar version[2]);

G_END_DECLS