#pragma once

#include <glib.h>

G_BEGIN_DECLS

gboolean skiu_file_set_nonblock (int fd);
gboolean skiu_file_set_block    (int fd);

G_END_DECLS