#include <signal.h>
#include <stdlib.h>
#include <glib.h>
#include <glib-unix.h>
#include <glib/gi18n.h>
#include "skiu-macros.h"

static void 
_exit_grateful (int signo)
{
	g_message (_("Exiting (on signal %d)..."), signo);
	exit (0);
}

G_GNUC_CONSTRUCTOR(P_SIGNAL) void 
skiu_signal_init (void)
{
	signal (SIGINT, _exit_grateful);
	signal (SIGTERM, _exit_grateful);
	signal (SIGPIPE, SIG_IGN);
	g_debug (_("Signal initialized."));
}

G_GNUC_DESTRUCTOR(P_SIGNAL) void 
skiu_signal_uninit (void)
{
	g_debug (_("Signal uninitialized."));
}