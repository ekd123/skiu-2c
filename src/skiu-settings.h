#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

GSettings *skiu_settings_get_server (void);

G_END_DECLS