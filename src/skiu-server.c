#include <bson.h>
#include <event.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include <gio/gnetworking.h>
#include "skiu-macros.h"
#include "skiu-settings.h"
#include "skiu-bson.h"
#include "skiu-client.h"
#include "skiu-user-mgmt.h"
#include "skiu-server.h"

/* list of connected clients */
static GSList            *clientlist    = NULL;
/* listen socket, used by server */
static GSocket           *listen_socket = NULL;
/* event_base, used by server */
static struct event_base *evbase        = NULL;
/* event struct for setting up listen fd, used by server */
static struct event       event         = { };

static void 
_on_accept_cb (int fd, short ev, gpointer user_data)
{
	GError *error = NULL;
	GSocket *clientsock = g_socket_accept (listen_socket, NULL, &error);
	SkiuClient *client = NULL;

	if (!clientsock || error)
	{
		g_critical (_("Couldn't accept one connection: %s"), error->message);
		g_error_free (error);
		return;
	}

	client = skiu_client_new (clientsock);
	if (!skiu_client_init_protocol (client) ||
		!skiu_client_init_version (client))
	{
		bson_t *err = skiu_bson_build (SK_TYPE_ERROR, 
			"n", G_TYPE_INT, SK_ERROR_VERSION_UNSUPPORTED, 
			"d", G_TYPE_STRING, _("Version not supported."),
			NULL);
		skiu_client_send_doc (client, err);
		bson_destroy (err);
		g_object_unref0 (client);
		return;
	}
	if (client)
	{
		/* Let him offline */
		bson_t *doc = skiu_bson_build (SK_TYPE_STATUS_GOOFFLINE, NULL);
		skiu_client_send_doc (client, doc);
		bson_destroy (doc);
		/* We (#SkiuServer) take over the ownership of @client */
		skiu_server_listener_add (client);
	}
}

static void 
_on_client_read_cb (int fd, short ev, gpointer user_data)
{
	SkProtocolDocType type;
	bson_iter_t _iter, *iter = &_iter;
	bson_bool_t reached_eof = FALSE;
	bson_reader_t _reader, *reader = &_reader;
	const bson_t *doc = NULL;
	SkiuClient *client = SKIU_CLIENT (user_data);

	bson_reader_init_from_fd (reader, fd, FALSE);
	doc = bson_reader_read (reader, &reached_eof);
	if (!doc || reached_eof)
	{
		bson_reader_destroy (reader);
		g_object_unref0 (client);
		return;
	}

	bson_iter_init_find (iter, doc, "t");
	type = bson_iter_int32 (iter);
	switch (type)
	{
	case SK_TYPE_STATUS_GOONLINE:
	case SK_TYPE_STATUS_GOOFFLINE:
	case SK_TYPE_STATUS_GOAWAY:
	case SK_TYPE_STATUS_GOBUSY:
	case SK_TYPE_STATUS_GOINVISIBLE:
	case SK_TYPE_STATUS_GOTALKTOME:
	{
		bson_t *doc = skiu_bson_build (type, NULL);
		skiu_client_send_doc (client, doc);
		bson_destroy (doc);
		break;
	}
	case SK_TYPE_INVALID:
	case SK_TYPE_ERROR:
	{
		bson_t *doc = skiu_bson_build (SK_TYPE_ERROR, 
			"n", G_TYPE_INT, SK_ERROR_BAD_DOC, 
			"d", G_TYPE_STRING, _("Bad document given"), 
			NULL);
		skiu_client_send_doc (client, doc);
		bson_destroy (doc);
		break;
	}
	default:
		g_assert_not_reached ();
		break;
	}
	bson_reader_destroy (reader);
}

G_GNUC_CONSTRUCTOR(P_SERVER) void 
skiu_server_init (void)
{
	GSocketAddress *sockaddr = NULL;
	GInetAddress *addr = NULL;
	GError *error = NULL;

	/* libevent */
	event_enable_debug_mode ();
	evbase = event_base_new ();
	/* gio networking */
	g_networking_init ();
	/* start listening */
	listen_socket = g_socket_new (G_SOCKET_FAMILY_IPV4, G_SOCKET_TYPE_STREAM, 
		G_SOCKET_PROTOCOL_TCP, &error);
	if (!listen_socket || error)
	{
		g_error (_("Couldn't create a socket for listening: %s"), 
			error->message);
		g_error_free (error);
	}

	addr = g_inet_address_new_any (G_SOCKET_FAMILY_IPV4);
	sockaddr = g_inet_socket_address_new (addr, g_settings_get_int (
		skiu_settings_get_server (), "port"));
	if (!g_socket_bind (listen_socket, sockaddr, TRUE, &error))
	{
		g_error (_("Couldn't bind: %s"), error->message);
		g_error_free (error);
	}
	g_object_unref0 (addr);

	if (!g_socket_listen (listen_socket, &error))
	{
		g_error (_("Couldn't listen on socket: %s"), error->message);
		g_error_free (error);
	}

	event_assign (&event, evbase, g_socket_get_fd (listen_socket), 
		EV_READ|EV_PERSIST, _on_accept_cb, NULL);
	event_add (&event, NULL);

	g_debug (_("Server initialized."));

	event_base_dispatch (evbase);
}

G_GNUC_DESTRUCTOR(P_SERVER) void 
skiu_server_uninit (void)
{
	g_slist_free_full (clientlist, g_object_unref);
	g_object_unref (listen_socket);
	event_base_loopbreak (evbase);
	event_base_free (evbase);
	g_debug (_("Server uninitialized."));
}

static void 
_skiu_server_listener_remove (SkiuClient *client, gpointer user_data)
{
	struct event *ev = (struct event *)user_data;
	event_del (ev);
	g_free (ev);
	clientlist = g_slist_remove (clientlist, client);
	g_message (_("One client quited."));
}

void 
skiu_server_listener_add (SkiuClient *client)
{
	g_assert (SKIU_IS_CLIENT (client));

	GSocket *socket = NULL;
	struct event *ev_read = g_malloc0 (sizeof (struct event));

	g_object_get (client, "socket", &socket, NULL);
	event_assign (ev_read, evbase, g_socket_get_fd (socket), EV_READ|EV_PERSIST,
		_on_client_read_cb, client);
	event_add (ev_read, NULL);

	/* take over the ownership of @client */
	clientlist = g_slist_append (clientlist, client);

	/* delete libevent binding when @client closes */
	g_signal_connect (client, "closed", G_CALLBACK (
		_skiu_server_listener_remove), ev_read);
	g_message (_("One new client joined."));
}