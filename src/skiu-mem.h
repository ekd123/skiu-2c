#pragma once

G_BEGIN_DECLS

gpointer _skiu_mem_delay_free_full (gpointer mem, guint timeout);
gpointer _skiu_mem_delay_unref_full (gpointer mem, guint timeout);

gpointer _skiu_mem_delay_free (gpointer mem);
gpointer _skiu_mem_delay_unref (gpointer mem);

G_END_DECLS

/**
 * skiu_mem_delay_free:
 * @m: data to be freed.
 * 
 * For details, see _skiu_mem_delay_free().
 */
#define skiu_mem_delay_free(m) ((typeof(m))_skiu_mem_delay_free(m))
/**
 * skiu_mem_delay_unref:
 * @m: object to be unref'd.
 * 
 * For details, see _skiu_mem_delay_unref().
 */
#define skiu_mem_delay_unref(m) ((typeof(m))_skiu_mem_delay_unref(m))
