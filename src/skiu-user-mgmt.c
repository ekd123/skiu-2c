#include "skiu-macros.h"
#include "skiu-protocol.h"
#include "skiu-client.h"
#include "skiu-user-mgmt.h"

//static GList *_avail_users = NULL;

G_GNUC_CONSTRUCTOR(P_USER_MGMT) void 
skiu_user_mgmt_init (void)
{
}

//static void 
//_skiu_user_mgmt_avail_remove (SkiuClient *client)
//{
//	_avail_users = g_list_remove (_avail_users, client);
//}

/**
 * skiu_user_mgmt_avail_add:
 * @client: a client
 * 
 * Mark @client as available, ie. @client is not offline.
 * This function increases the refcount of @client.
 * 
 * When @client disconnects, the entry will be deleted automatically.
 */
void 
skiu_user_mgmt_avail_add (SkiuClient *client)
{
//	_avail_users = g_list_append (_avail_users, client);
//	g_signal_connect (client, "closed", G_CALLBACK (
//		_skiu_user_mgmt_avail_remove), NULL);
}