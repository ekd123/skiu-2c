#include <locale.h>
#include <glib.h>

/**
 * SECTION: SkiuMain
 * 
 * This part initializes the execution.
 * All the functions which have "_init" suffix will automatically run at boot.
 */

gint main (gint argc, gchar *argv[])
{
	setlocale (LC_ALL, "");
	return 0;
}
