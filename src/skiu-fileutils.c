#include <fcntl.h>
#include "skiu-fileutils.h"

/**
 * skiu_file_set_nonblock:
 * @fd: fd to be set
 * 
 * Set @fd as nonblocking.
 * 
 * Returns: %TRUE on success.
 */
gboolean 
skiu_file_set_nonblock (int fd)
{
	int flags;
	flags = fcntl (fd, F_GETFL);
	if (flags < 0)
		return FALSE;
	flags |= O_NONBLOCK;
	if (fcntl (fd, F_SETFL, flags) < 0)
		return FALSE;
	return TRUE;
}

/**
 * skiu_file_set_block:
 * @fd: fd to be set
 * 
 * Set @fd as blocking.
 * 
 * Returns: %TRUE on success.
 */
gboolean 
skiu_file_set_block (int fd)
{
	int flags;
	flags = fcntl (fd, F_GETFL);
	if (flags < 0)
		return FALSE;
	flags &= ~O_NONBLOCK;
	if (fcntl (fd, F_SETFL, flags) < 0)
		return FALSE;
	return TRUE;
}