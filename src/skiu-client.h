/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * skiu-client.h
 *
 */

#ifndef _SKIU_CLIENT_H_
#define _SKIU_CLIENT_H_

#include <bson.h>
#include <gio/gio.h>

G_BEGIN_DECLS

#define SKIU_TYPE_CLIENT             (skiu_client_get_type ())
#define SKIU_CLIENT(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), SKIU_TYPE_CLIENT, SkiuClient))
#define SKIU_CLIENT_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), SKIU_TYPE_CLIENT, SkiuClientClass))
#define SKIU_IS_CLIENT(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SKIU_TYPE_CLIENT))
#define SKIU_IS_CLIENT_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), SKIU_TYPE_CLIENT))
#define SKIU_CLIENT_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), SKIU_TYPE_CLIENT, SkiuClientClass))

typedef struct _SkiuClientClass SkiuClientClass;
typedef struct _SkiuClient SkiuClient;
typedef struct _SkiuClientPrivate SkiuClientPrivate;

struct _SkiuClientClass
{
	GObjectClass parent_class;
};

struct _SkiuClient
{
	GObject parent_instance;

	SkiuClientPrivate *priv;
};

GType       skiu_client_get_type      (void) G_GNUC_CONST;
SkiuClient *skiu_client_new           (GSocket      *socket);
gboolean    skiu_client_init_protocol (SkiuClient   *client);
gboolean    skiu_client_init_version  (SkiuClient   *client);
gboolean    skiu_client_send_doc      (SkiuClient   *client,
                                       const bson_t *doc);

G_END_DECLS

#endif /* _SKIU_CLIENT_H_ */

