#include <glib/gi18n.h>
#include <gio/gio.h>
#include "skiu-macros.h"
#include "skiu-bson.h"
#include "skiu-protocol.h"
#include "skiu-client.h"

/**
 * SECTION: SkiuClient
 * @include: skiu-client.h
 * @short_description: Class for clients
 * 
 * Clients are handled here, and when the connection is closed, ::closed will 
 * be emitted.
 * 
 * Note that setting/getting properties here will always be non-duplicated, 
 * don't free them.
 */

struct _SkiuClientPrivate
{
	SkProtocolDocType status;
	gchar *assd_id;
	gchar *signature;
	gchar *host;
	GSocket *socket;
};

enum
{
	PROP_0, 

	PROP_HOST, 
	PROP_SOCKET, 
	PROP_ASSDID, 
	PROP_SIGNATURE, 
	PROP_STATUS, 
};

enum
{
	SIGNAL_CLOSED,

	N_SIGNALS
};

static guint client_signals[N_SIGNALS] = { 0 };

G_DEFINE_TYPE (SkiuClient, skiu_client, G_TYPE_OBJECT);

static void 
skiu_client_init (SkiuClient *skiu_client)
{
	skiu_client->priv = G_TYPE_INSTANCE_GET_PRIVATE (skiu_client, 
		SKIU_TYPE_CLIENT, SkiuClientPrivate);
	skiu_client->priv->socket = NULL;
}

static void 
skiu_client_finalize (GObject *object)
{
	SkiuClient *client = SKIU_CLIENT (object);
	g_free0 (client->priv->host);
	g_free0 (client->priv->assd_id);
	g_free0 (client->priv->signature);
	g_signal_emit (client, client_signals[SIGNAL_CLOSED], 0);
	G_OBJECT_CLASS (skiu_client_parent_class)->finalize (object);
}

static void 
skiu_client_dispose (GObject *object)
{
	SkiuClient *client = SKIU_CLIENT (object);
	g_assert (client->priv->socket);
	g_object_unref0 (client->priv->socket);
	G_OBJECT_CLASS (skiu_client_parent_class)->finalize (object);
}

static void 
skiu_client_set_property (GObject *object, guint prop_id, const GValue *value, 
	GParamSpec *pspec)
{
	SkiuClient *client = SKIU_CLIENT (object);
	g_return_if_fail (SKIU_IS_CLIENT (object));

	switch (prop_id)
	{
	case PROP_HOST:
		g_free0 (client->priv->host);
		client->priv->host = g_value_dup_string (value);
		break;
	case PROP_SOCKET:
		g_object_unref0 (client->priv->socket);
		client->priv->socket = g_value_get_object (value);
		break;
	case PROP_ASSDID:
		g_free0 (client->priv->assd_id);
		client->priv->assd_id = g_value_dup_string (value);
		break;
	case PROP_SIGNATURE:
		g_free0 (client->priv->signature);
		client->priv->signature = g_value_dup_string (value);
		break;
	case PROP_STATUS:
		client->priv->status = g_value_get_int (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void 
skiu_client_get_property (GObject *object, guint prop_id, GValue *value, 
	GParamSpec *pspec)
{
	SkiuClient *client = SKIU_CLIENT (object);
	g_return_if_fail (SKIU_IS_CLIENT (object));

	switch (prop_id)
	{
	case PROP_HOST:
		g_value_take_string (value, client->priv->host);
		break;
	case PROP_SOCKET:
		g_value_take_object (value, client->priv->socket);
		break;
	case PROP_ASSDID:
		g_value_take_string (value, client->priv->assd_id);
		break;
	case PROP_SIGNATURE:
		g_value_take_string (value, client->priv->signature);
		break;
	case PROP_STATUS:
		g_value_set_int (value, client->priv->status);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void 
skiu_client_class_init (SkiuClientClass *class)
{
	GObjectClass* object_class = G_OBJECT_CLASS (class);

	g_type_class_add_private (class, sizeof (SkiuClientPrivate));

	object_class->finalize = skiu_client_finalize;
	object_class->dispose = skiu_client_dispose;
	object_class->set_property = skiu_client_set_property;
	object_class->get_property = skiu_client_get_property;

	g_object_class_install_property (object_class,
		PROP_HOST, g_param_spec_string ("host", "host",  
			"IP address of the client", "", 
			G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT));
	g_object_class_install_property (object_class,
		PROP_SOCKET, g_param_spec_object ("socket", "socket", 
			"Socket for connection", G_TYPE_SOCKET, 
			G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (object_class, 
		PROP_ASSDID, g_param_spec_string ("assd_id", "assd_id", 
			"Associated ID", "", 
			G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property (object_class,
		PROP_SIGNATURE, g_param_spec_string ("signature", "signature",  
			"Signature text", "", 
			G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property (object_class,
		PROP_STATUS, g_param_spec_int ("status", "status",  
			"Current status", SK_TYPE_STATUS_GOONLINE, SK_TYPE_STATUS_GOOFFLINE,
			SK_TYPE_STATUS_GOOFFLINE, G_PARAM_READABLE | G_PARAM_WRITABLE));

	client_signals[SIGNAL_CLOSED] =
		g_signal_new ("closed", 
			G_OBJECT_CLASS_TYPE (class), 
			G_SIGNAL_RUN_FIRST, 
			0, NULL, NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0);
}

/**
 * skiu_client_new:
 * @socket: base #GSocket
 * 
 * Create a #SkiuClient for @socket.
 * 
 * Returns: (transfer-full): a new #SkiuClient.
 */
SkiuClient * 
skiu_client_new (GSocket *socket)
{
	return g_object_new (SKIU_TYPE_CLIENT, 
		"socket", socket, 
		NULL);
}

gboolean 
skiu_client_init_protocol (SkiuClient *client)
{
	gboolean result;
	gchar buf;

	g_return_val_if_fail (SKIU_IS_CLIENT (client), FALSE);

	g_socket_set_timeout (client->priv->socket, 1);
	g_socket_receive_with_blocking (client->priv->socket, &buf, sizeof (buf), 
		TRUE, NULL, NULL);
	if (buf != 0x02)
		result = FALSE;
	else 
		result = TRUE;
	g_socket_set_timeout (client->priv->socket, 0);
	return result;
}

gboolean 
skiu_client_init_version (SkiuClient *client)
{
	gboolean result;
	gchar buf[2];

	g_return_val_if_fail (SKIU_IS_CLIENT (client), FALSE);

	g_socket_set_timeout (client->priv->socket, 1);
	g_socket_receive_with_blocking (client->priv->socket, buf, sizeof (buf), 
		TRUE, NULL, NULL);
	if (!skiu_protocol_supported (buf))
		result = FALSE;
	else 
		result = TRUE;
	g_socket_set_timeout (client->priv->socket, 0);
	return result;
}

gboolean 
skiu_client_send_doc (SkiuClient *client, const bson_t *doc)
{
	GError *error = NULL;

	g_return_val_if_fail (SKIU_IS_CLIENT (client), FALSE);

	if (g_socket_send (client->priv->socket, (gchar *)bson_get_data (doc), 
		doc->len, NULL, &error) != doc->len)
	{
		g_warning (_("Data sent to 0x%lX wasn't complete: %s"), (gulong)client, 
			error->message);
		g_error_free (error);
		return FALSE;
	}
	return TRUE;
}

void 
skiu_client_change_status (SkiuClient *client, SkProtocolDocType status, 
	const gchar *signature)
{
	bson_t *doc = skiu_bson_build (status, "s", G_TYPE_STRING, signature);
	g_object_set (client, "status", status, "signature", signature, NULL);
	skiu_client_send_doc (client, doc);
	bson_destroy (doc);
}
