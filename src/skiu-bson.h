#pragma once

#include <stdarg.h>
#include <bson.h>
#include <glib.h>
#include "skiu-protocol.h"

G_BEGIN_DECLS

bson_t *skiu_bson_build_valist (SkProtocolDocType doctype, 
                                const gchar *key, 
	                            va_list args);
bson_t *skiu_bson_build        (SkProtocolDocType doctype,
                                const gchar *key,
                                ...);

G_END_DECLS