#pragma once

/* priority for constructor/destructor */
#define P_SETTINGS 210
#define P_PID 220
#define P_USER_MGMT 230
#define P_SIGNAL 240
#define P_SERVER 250

/**
 * G_GNUC_CONSTRUCTOR:
 * @p: priority, the lower it is, the earlier it runs
 * 
 * Define a constructor function, which will be run automatically on program's 
 * boot.
 */
#define G_GNUC_CONSTRUCTOR(p) __attribute__((constructor(p)))

/**
 * G_GNUC_CONSTRUCTOR:
 * @p: priority, the larger it is, the earlier it runs
 * 
 * Define a destructor function, which will be run automatically on program's 
 * boot.
 */
#define G_GNUC_DESTRUCTOR(p) __attribute__((destructor(p)))

#define g_object_unref0(o) if(o)g_object_unref(o);o=NULL
#define g_free0(o) if(o)g_free(o);o=NULL

/**
 * nofree:
 * 
 * Declare one variable shouldn't be freed, even though it's not a const 
 * variable. This mostly happens where one variable can't be a const due to 
 * function argument types.
 */
#define nofree
