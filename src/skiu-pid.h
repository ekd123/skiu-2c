#pragma once

G_BEGIN_DECLS

gchar    *skiu_pid_read   (void);
void      skiu_pid_write  (void);
gboolean  skiu_pid_exists (void);

G_END_DECLS