#include <glib-object.h>
#include <glib/gi18n.h>
#include "skiu-macros.h"

static gboolean
_free_func (gpointer data)
{
	g_free0 (data);
	return FALSE;
}

static gboolean
_unref_func (gpointer data)
{
	g_object_unref0 (data);
	return FALSE;
}

gpointer 
_skiu_mem_delay_free_full (gpointer mem, guint timeout)
{
	g_timeout_add (timeout, _free_func, mem);
	return mem;
}

/**
 * _skiu_mem_delay_free:
 * @mem: data to be freed
 * 
 * Free @mem 10sec later.
 * 
 * Returns: @mem
 */
gpointer 
_skiu_mem_delay_free (gpointer mem)
{
	return _skiu_mem_delay_free_full (mem, 10*1000);
}

gpointer 
_skiu_mem_delay_unref_full (gpointer mem, guint timeout)
{
	g_timeout_add (timeout, _unref_func, mem);
	return mem;
}

/**
 * _skiu_mem_delay_free:
 * @mem: object to be unref'd
 * 
 * Free @mem 10sec later.
 * Use skiu_mem_delay_free() instead.
 * 
 * Returns: @mem
 */
gpointer 
_skiu_mem_delay_unref (gpointer mem)
{
	return _skiu_mem_delay_unref_full (mem, 10*1000);
}
