#include <string.h>
#include "skiu-protocol.h"

gchar supported_versions[][2] = 
{
	{ 0x1E, 0x0D }, 
	{ 0x0, 0x0 }
};

gboolean 
skiu_protocol_supported (gchar version[2])
{
	for (gint i = 0; supported_versions[i][0] != 0; i ++)
	{
		if (strncmp (version, supported_versions[i], 2) == 0)
			return TRUE;
	}
	return FALSE;
}
