#include <glib/gi18n.h>
#include <gio/gio.h>
#include "skiu-macros.h"

static GSettings *_server_settings = NULL;

G_GNUC_CONSTRUCTOR(P_SETTINGS) void 
skiu_settings_init (void)
{
	_server_settings = g_settings_new ("org.ekd123.SKiu.server");
	g_debug (_("Settings initialized."));
}

G_GNUC_DESTRUCTOR(P_SETTINGS) void 
skiu_settings_uninit (void)
{
	g_settings_sync ();
	g_object_unref (_server_settings);
	g_debug (_("Settings uninitialized."));
}

/**
 * skiu_get_server_settings:
 * 
 * Get the #GSettings of server itself.
 * 
 * Returns: (transfer-none): the #GSettings.
 */
GSettings * 
skiu_settings_get_server (void)
{
	return _server_settings;
}