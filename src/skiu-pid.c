#include <sys/types.h>
#include <unistd.h>
#include <glib/gi18n.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include "skiu-macros.h"
#include "skiu-settings.h"
#include "skiu-mem.h"
#include "skiu-pid.h"

G_GNUC_CONSTRUCTOR(P_PID) void 
skiu_pid_init (void)
{
	if (skiu_pid_exists () == TRUE)
	{
		gchar *pid = skiu_pid_read ();
		g_error (_("Another instance (pid: %s) is running"), pid);
		g_free (pid);
	}
	else
	{
		skiu_pid_write ();
	}
	g_debug (_("Pid initialized."));
}

G_GNUC_DESTRUCTOR(P_PID) void 
skiu_pid_uninit (void) 
{
	gchar *pidpath = g_settings_get_string (skiu_settings_get_server (), 
		"pidpath");
	g_unlink (pidpath);
	g_free0 (pidpath);
	g_debug (_("Pid uninitialized."));
}

gboolean 
skiu_pid_exists (void)
{
	nofree GSettings *settings = skiu_settings_get_server ();
	gchar *pidpath = g_settings_get_string (settings, "pidpath");

	if (g_file_test (pidpath, G_FILE_TEST_EXISTS) == TRUE)
		return g_free (pidpath), TRUE;
	return g_free (pidpath), FALSE;
}

gchar * 
skiu_pid_read (void)
{
	GError *error = NULL;
	nofree GSettings *settings = skiu_settings_get_server ();
	gchar *pidpath = g_settings_get_string (settings, "pidpath");
	GFile *file = g_file_new_for_path (pidpath);
	GInputStream *basestream = (GInputStream *)g_file_read (file, NULL, &error);
	GDataInputStream *dis = NULL;
	gchar *line = NULL;

	if (!basestream || error)
	{
		g_error (_("Couldn't read the pid file but it exists: %s"), 
			error->message);
		g_error_free (error);
		goto lbl_cleanup;
	}

	dis = g_data_input_stream_new (basestream);
	line = g_data_input_stream_read_line (dis, NULL, NULL, &error);
	if (!line || error)
	{
		g_error (_("Couldn't read the pid file but it exists: %s"), 
			error->message);
		g_error_free (error);
		goto lbl_cleanup;
	}

lbl_cleanup:
	g_free0 (pidpath);
	g_object_unref0 (file);
	g_object_unref0 (basestream);
	g_object_unref0 (dis);
	return line;
}

void 
skiu_pid_write (void)
{
	GError *error = NULL;
	nofree GSettings *settings = skiu_settings_get_server ();
	gchar *pidpath = g_settings_get_string (settings, "pidpath");
	GFile *file = g_file_new_for_path (pidpath);
	GOutputStream *os = (GOutputStream *)g_file_create (file, 
			G_FILE_CREATE_NONE, NULL, &error);
	GDataOutputStream *dos = NULL;
	gchar *contents = NULL;

	if (!os || error)
	{
		g_error (_("Couldn't open %s for writing: %s"), pidpath, 
			error->message);
		g_error_free (error);
		goto lbl_cleanup;
	}

	dos = g_data_output_stream_new (os);
	contents = g_strdup_printf ("%d\n", getpid ());
	g_data_output_stream_put_string (dos, contents, NULL, &error);
	if (error)
	{
		g_error (_("Couldn't write to %s: %s"), pidpath, error->message);
		g_error_free (error);
		goto lbl_cleanup;
	}
lbl_cleanup:
	g_free0 (pidpath);
	g_free0 (contents);
	g_object_unref0 (file);
	g_object_unref0 (os);
	g_object_unref0 (dos);
}