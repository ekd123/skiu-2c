#include <glib-object.h>
#include "skiu-bson.h"

bson_t * 
skiu_bson_build_valist (SkProtocolDocType doctype, const gchar *first_key, 
	va_list args)
{
	bson_t *doc = bson_new ();
	bson_append_int32 (doc, "t", 1, doctype);
	if (first_key)
	{
		const gchar *key = first_key;
		do 
		{
			const gchar *vstring;
			gint32 vint32;
			gint64 vint64;

			GType datatype;

			datatype = va_arg (args, GType);
			switch (datatype)
			{
			case G_TYPE_STRING:
				vstring = va_arg (args, const gchar *);
				bson_append_utf8 (doc, key, 1, vstring, -1);
				break;
			case G_TYPE_INT:
				vint32 = va_arg (args, gint32);
				bson_append_int32 (doc, key, 1, vint32);
				break;
			case G_TYPE_INT64:
				vint64 = va_arg (args, gint64);
				bson_append_int64 (doc, key, 1, vint64);
				break;
			default:
				g_assert_not_reached ();
				break;
			}
		} while ((key = va_arg (args, const gchar *)));
	}
	return doc;
}

bson_t * 
skiu_bson_build (SkProtocolDocType doctype, const gchar *key, ...)
{
	bson_t *result;
	va_list args;
	va_start (args, key);
	result = skiu_bson_build_valist (doctype, key, args);
	va_end (args);
	return result;
}